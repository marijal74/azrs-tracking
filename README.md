# azrs - tracking
---

Repozitorijum za testiranje alata radjenih na kursu Alati za razvoj softvera.

## Testirani alati:

* [ Meld merge tool](https://gitlab.com/marijal74/azrs-tracking/-/issues/1)
* [CMake](https://gitlab.com/marijal74/azrs-tracking/-/issues/2)
* [Callgrind](https://gitlab.com/marijal74/azrs-tracking/-/issues/8)
* [ClangFormat](https://gitlab.com/marijal74/azrs-tracking/-/issues/4)
* [ClangTidy](https://gitlab.com/marijal74/azrs-tracking/-/issues/5)
* [Git hooks](https://gitlab.com/marijal74/azrs-tracking/-/issues/3)
* [Valgrind](https://gitlab.com/marijal74/azrs-tracking/-/issues/6)

Projekat na kome su testirani alati:  [Monopol](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/03-monopol)

Na projektu smo koristili Git sistem za verzionisanje i GitFlow.
